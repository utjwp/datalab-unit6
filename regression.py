import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error
import ast

# Paths to your CSV files
train_path = 'stolen_two_class_data_oKlPl0FQHg.csv'
test_path = 'stolen_two_class_data_Pji_viNFJQ.csv'

# Function to load and preprocess data from CSV
def load_data_from_csv(path):
    data = pd.read_csv(path)
    data['array'] = data['array'].apply(ast.literal_eval)  # Convert string to list
    return data

# Load and prepare the training data
train_data = load_data_from_csv(train_path)
train_images = np.array(train_data['array'].tolist())  # Convert list of lists to 2D numpy array
train_scores = train_data['score'].values

# Load and prepare the test data
test_data = load_data_from_csv(test_path)
test_images = np.array(test_data['array'].tolist())  # Convert list of lists to 2D numpy array
test_scores = test_data['score'].values

# Split the training data into training and validation sets
X_train, X_val, y_train, y_val = train_test_split(train_images, train_scores, test_size=0.2, random_state=42)

# Train a linear regression model
model = LinearRegression()
model.fit(X_train, y_train)

# Predict on the validation set
y_pred_val = model.predict(X_val)

# Compute and print the mean squared error on the validation set
mse_val = mean_squared_error(y_val, y_pred_val)
print(f'Validation MSE: {mse_val}')

# Predict on the test set
y_pred_test = model.predict(test_images)

# Compute and print the mean squared error on the test set
mse_test = mean_squared_error(test_scores, y_pred_test)
print(f'Test MSE: {mse_test}')
