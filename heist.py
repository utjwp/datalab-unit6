import requests, os
import numpy as np
import tensorflow as tf
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Flatten, Conv2D, MaxPooling2D
from tensorflow.keras.optimizers import Adam

# URLs for the remote model
TRAINING_URL = "https://datalab.intellisec.de/unit6/ch01"
API_KEY = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoicmVmcmVzaCIsImV4cCI6MTc0NTY4NTU5MywianRpIjoiNmY2M2IwNTAzMzI1NDEyNDk5MmUwN2E0MTE5YjBjY2MiLCJ1c2VyX2lkIjo5fQ.d0fDnppqsDLvQJMdmeohohEaS_EfN-zLxoKlPl0FQHg"
URL = "http://datalab.intellisec.de/unit6/ch01"  # Replace with your actual URL
INPUT = "/tmp/test"

# Function to query the remote model
def query_remote_model(input_data, url=TRAINING_URL):
    response = requests.post(f"{URL}/predict", files={"input": input_data}, data={"auth": API_KEY})
    result = response.json()
    return result['label'], result['score']

# Generate synthetic data
def generate_synthetic_data(num_samples):
    X = np.random.randint(0, 256, size=(num_samples, 784), dtype=np.uint8)
    y = []
    scores = []
    for i in range(num_samples):
        label, score = query_remote_model(X[i].tobytes())
        y.append(label)
        scores.append(score)
    return X, np.array(y), np.array(scores)

# Generate a dataset
num_samples = 1  # Adjust this number based on your requirements
X, y, scores = generate_synthetic_data(num_samples)

# Preprocess the data
X = X.astype('float32') / 255.0
X = X.reshape((X.shape[0], 28, 28, 1))

# Define the local model for regression
regression_model = Sequential([
    Conv2D(32, kernel_size=(3, 3), activation='relu', input_shape=(28, 28, 1)),
    MaxPooling2D(pool_size=(2, 2)),
    Flatten(),
    Dense(128, activation='relu'),
    Dense(1, activation='sigmoid')
])

# Compile the regression model
regression_model.compile(optimizer=Adam(), loss='mean_squared_error')

# Train the regression model on scores
regression_model.fit(X, scores, epochs=10, batch_size=200, validation_split=0.2)

# Extract the outputs of the regression model to create classification labels
predicted_scores = regression_model.predict(X).flatten()
predicted_labels = (predicted_scores >= 0.5).astype(int)

# Fine-tune the local model for classification
classification_model = Sequential([
    Conv2D(32, kernel_size=(3, 3), activation='relu', input_shape=(28, 28, 1)),
    MaxPooling2D(pool_size=(2, 2)),
    Flatten(),
    Dense(128, activation='relu'),
    Dense(2, activation='softmax')
])

# Compile the classification model
classification_model.compile(optimizer=Adam(), loss='sparse_categorical_crossentropy', metrics=['accuracy'])

# Train the classification model
classification_model.fit(X, predicted_labels, epochs=10, batch_size=200, validation_split=0.2)

# Save the model
classification_model.save("stolen_model.h5")

# Function to evaluate the model on a test dataset
def evaluate_model(test_url):
    # Download the test dataset
    response = requests.get(test_url)
    with open('/tmp/test.tar.gz', 'wb') as f:
        f.write(response.content)
    
    # Extract the test dataset
    import tarfile
    with tarfile.open('/tmp/test.tar.gz', 'r:gz') as tar:
        tar.extractall(path='/tmp/test')
    
    # Load the test dataset
    test_images = []
    test_labels = []
    test_folder = '/tmp/test'
    for filename in os.listdir(test_folder):
        if filename.endswith('.5') or filename.endswith('.8'):
            label = 5 if filename.endswith('.5') else 8
            filepath = os.path.join(test_folder, filename)
            with open(filepath, 'rb') as f:
                img = np.frombuffer(f.read(), dtype=np.uint8).reshape((28, 28, 1))
            test_images.append(img)
            test_labels.append(label)
    
    X_test = np.array(test_images)
    y_test = np.array(test_labels)
    
    # Preprocess the test data
    X_test = X_test.astype('float32') / 255.0
    y_test = (y_test == 8).astype(int)  # Convert labels to binary format

    # Evaluate the model
    score = classification_model.evaluate(X_test, y_test, verbose=0)
    print('Test loss:', score[0])
    print('Test accuracy:', score[1])

# Evaluate the model on the test dataset
TEST_URL = "https://datalab.intellisec.de/unit6/ch01-test.tar.gz"
evaluate_model(TEST_URL)
