import numpy as np
import pandas as pd
import ast
from sklearn.model_selection import train_test_split, GridSearchCV
from sklearn.metrics import mean_squared_error
import tensorflow as tf
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Flatten
from sklearn.metrics import accuracy_score
from tensorflow.keras.optimizers import Adam
from kerastuner.tuners import RandomSearch
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Flatten, Dense, Dropout


import os
import csv

# Paths to your CSV files
train_path = ['stolen_multiclass_data_o8HrCSxLUk.csv']
test_datalab_path = "unit6-ch02-test"


def load_data_from_csv(paths):
    data = pd.DataFrame()
    for path in paths:
        data_1 = pd.read_csv(path)
        data_1['array'] = data_1['array'].apply(ast.literal_eval)  # Convert string to list
        data = pd.concat([data, data_1], ignore_index=True)
    return data

train_data = load_data_from_csv(train_path)
train_images = np.array(train_data['array'].tolist())  # Convert list of lists to 2D numpy array
train_images = train_images.reshape(-1, 28, 28, 1) / 255.0
train_scores = train_data['label'].values

train_images = train_images / 255.0

# Split the training data into training and validation sets
X_train, X_val, y_train, y_val = train_test_split(train_images, train_scores, test_size=0.2, random_state=42)


conv_units_1 = 128
conv_units_2 = 32
dense_units = 160
learning_rate = 0.001

def build_model():
    model = Sequential()
    model.add(Conv2D(conv_units_1, kernel_size=(3, 3), activation='relu', input_shape=(28, 28, 1)))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Conv2D(conv_units_2, kernel_size=(3, 3), activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Flatten())
    model.add(Dense(dense_units, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(10, activation='softmax'))  # Output layer for 10 classes

    model.compile(optimizer=Adam(learning_rate=learning_rate),
                  loss='sparse_categorical_crossentropy',
                  metrics=['accuracy'])
    return model

# Build and train the model
model = build_model()
model.fit(X_train, y_train, epochs=20, validation_data=(X_val, y_val))
# Train the best model on the full training data

# Evaluate the best model on the validation data
val_predictions = model.predict(X_val)
val_predictions_classes = np.argmax(val_predictions, axis=1)
val_accuracy = accuracy_score(y_val, val_predictions_classes)
print(f'Validation Accuracy: {val_accuracy}')


# classify test data
def get_image_bytes(image_path):
    with open(image_path, 'rb') as f:
        img_bytes = f.read()
        array = np.frombuffer(img_bytes, dtype=np.uint8)
        return array
    
def load_images_from_folder(folder):
    images = []
    filenames = []
    for filename in os.listdir(folder):
        img = get_image_bytes(os.path.join(folder, filename))
        if len(img) != 784:
                print(f"Error: The input image {filename} does not have 784 bytes.")
                continue
        images.append(img)
        filenames.append(filename)
            
    return images, filenames


images, filenames = load_images_from_folder(test_datalab_path)
images = np.array(images).reshape(-1, 28, 28, 1) / 255.0
pred_scores = model.predict(images)
pred_classes = np.argmax(pred_scores, axis=1)

with open("output2.csv", 'w', newline='') as csvfile:
    csvwriter = csv.writer(csvfile)
    for i, prediction in enumerate(pred_classes):
        csvwriter.writerow([filenames[i], prediction])

