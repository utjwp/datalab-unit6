import numpy as np
from scipy.optimize import lsq_linear
import os
import numpy as np
from skimage.io import imread
from skimage.color import rgb2gray
from skimage.transform import resize
from sklearn.svm import SVC
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report, accuracy_score
from sklearn.metrics import mean_squared_error
from sklearn.metrics.pairwise import rbf_kernel
import cv2


def load_images_from_folder(folder):
    images = []
    labels = []
    for filename in os.listdir(folder):
        if filename.endswith('.8') or filename.endswith('.5'):
            # Load the image
            # img = imread(os.path.join(folder, filename))
            # #img = rgb2gray(img)  # Convert to grayscale
            # img = resize(img, (28, 28))  # Resize to 28x28 pixels
            # img = img.flatten()  # Flatten the image to a 1D array of 784 pixels
            img = cv2.imread(os.path.join(folder, filename), cv2.IMREAD_GRAYSCALE)

            images.append(img)
            
            # Assign label based on filename
            if filename.endswith('.8'):
                labels.append(8)
            elif filename.endswith('.5'):
                labels.append(5)
    
    all_images = np.array(images)
    #all_images = all_images.reshape((all_images.shape[0], 28, 28, 1))
    all_images = all_images.flatten().reshape(all_images.shape[0], -1)
    all_images = all_images.astype('float32') / 255
    return all_images, np.array(labels)

def train_svm(images, labels):
    # Split the data into training and testing sets
    X_train, X_test, y_train, y_test = train_test_split(images, labels, test_size=0.2, random_state=42)
    
    # Train the SVM model
    svm_model = SVC(kernel='rbf', random_state=42)
    svm_model.fit(X_train, y_train)
    
    # Evaluate the model
    y_pred = svm_model.predict(X_test)
    print("Classification Report:\n", classification_report(y_test, y_pred))
    print("Accuracy:", accuracy_score(y_test, y_pred))
    
    return svm_model

# Example usage
folder_path = '/Users/sascharolinger/dev/datalab/datalab-unit6/ch01_train'  # Replace with the path to your folder containing images

images, labels = load_images_from_folder(folder_path)
svm_model = train_svm(images, labels)

def api_query(svm_model, sample):
    # Query the SVM model for the decision function value and label
    sample = sample.reshape(1, -1)
    # Query the SVM model for the decision function value and label
    decision_function_value = svm_model.decision_function(sample)[0]
    label = svm_model.predict(sample)[0]
    return decision_function_value, label


# Step 2: Collect samples
def collect_samples(num_samples):
    samples = []
    for _ in range(num_samples):
        sample = np.random.randint(0, 256, size=784, dtype=np.uint8)
        # Query the model
        decision_function_value, label = api_query(svm_model, sample)
        samples.append((sample, decision_function_value, label))
    return samples

# Step 3: Solve for weights and bias
def solve_for_weights_and_bias(samples):
    X = []
    y = []
    for sample, decision_function_value, label in samples:
        X.append(sample)
        y.append(decision_function_value)
    
    X = np.array(X)
    y = np.array(y)
    
    # Add a column of ones to X for the bias term
    X = np.hstack((X, np.ones((X.shape[0], 1))))
    
    # Solve the linear system using least squares
    result = lsq_linear(X, y)
    
    weights_bias = result.x
    weights = weights_bias[:-1]
    bias = weights_bias[-1]
    
    return weights, bias

def solve_for_weights_and_bias_rbf(samples, gamma=1):
    X = []
    y = []
    for sample, decision_function_value, label in samples:
        X.append(sample)
        y.append(decision_function_value)
    
    X = np.array(X)
    y = np.array(y)
    
    # Compute the RBF kernel matrix
    K = rbf_kernel(X, gamma=gamma)
    
    # Solve the linear system using least squares
    # Add a column of ones to K for the bias term
    K = np.hstack((K, np.ones((K.shape[0], 1))))
    
    result = lsq_linear(K, y)
    
    weights_bias = result.x
    weights = weights_bias[:-1]
    bias = weights_bias[-1]
    
    return weights, bias


# Main function to steal the model
def steal_svm_model(num_samples):
    samples = collect_samples(num_samples)
    weights, bias = solve_for_weights_and_bias(samples)
    return weights, bias


def steal_svm_model_rbf(num_samples):
    samples = collect_samples(num_samples)
    weights, bias = solve_for_weights_and_bias_rbf(samples)
    return weights, bias

# Example usage
num_samples = 1000  # Number of samples to collect
weights, bias = steal_svm_model_rbf(num_samples)
print("Weights:", weights)
print("Bias:", bias)

#coefficients = svm_model.coef_

# Intercept (bias) added to the decision function
#intercept = svm_model.intercept_

print("Coefficients (Weights):")
#print(coefficients)
print("Intercept (Bias):")
#print(intercept)

#weights = svm_model.coef_.flatten()  # Assuming SVM is linear, weights are coef_
#bias = svm_model.intercept_[0]  # Bias is intercept_

# Create a new SVM model with the stolen weights and bias
stolen_svm_model = SVC(kernel='linear', random_state=42, coef0=bias)
stolen_svm_model.coef_ = np.array([weights])
stolen_svm_model.intercept_ = np.array([bias])

X_test = images[-100:]  # Example: Use the last 100 images as test data
y_test = labels[-100:]

# Original SVM predictions
y_pred_original = svm_model.decision_function(X_test)

# Stolen SVM predictions
#y_pred_stolen = np.dot(X_test, weights) + bias

def set_up_svm_with_stolen_params(weights, bias, gamma=1.0):
    svm_model = SVC(kernel='rbf', gamma=gamma)
    
    # Set weights and bias
    n_support_vectors = len(weights)
    svm_model.dual_coef_ = np.array([weights])
    svm_model.intercept_ = np.array([bias])
    svm_model.support_vectors_ = np.random.rand(n_support_vectors, 784)  # Example: Replace with actual support vectors
    
    return svm_model

def approximate_decision_function_values_rbf(svm_model, X_test, gamma=1.0):
    decision_function_values = []
    for i in range(len(X_test)):
        kernel_values = rbf_kernel(svm_model.support_vectors_, X_test[i].reshape(1, -1), gamma=gamma).flatten()
        decision_function_value = np.dot(kernel_values, svm_model.dual_coef_.ravel()) + svm_model.intercept_[0]
        decision_function_values.append(decision_function_value)
    return np.array(decision_function_values)

# Example usage


# Assuming svm_model is your trained SVM model with an RBF kernel
#y_pred_stolen = approximate_decision_function_values_rbf(svm_model, X_test, gamma=0.1)
svm_model_stolen = set_up_svm_with_stolen_params(weights, bias, gamma=1.0)
y_pred_stolen = svm_model_stolen.decision_function(X_test)

print("Stolen Decision Function Values:", y_pred_stolen)

# Compute MSE
mse = mean_squared_error(y_pred_original, y_pred_stolen)
print("mse:")
print(mse)