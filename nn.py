import numpy as np
import pandas as pd
import ast
from sklearn.model_selection import train_test_split, GridSearchCV
from sklearn.metrics import mean_squared_error
import tensorflow as tf
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Flatten
from tensorflow.keras.optimizers import Adam
from kerastuner.tuners import RandomSearch

import os
import csv

# Paths to your CSV files
train_path = ['stolen_two_class_data_oKlPl0FQHg.csv', 'stolen_two_class_data_Pji_viNFJQ.csv' ]
test_path = ['stolen_two_class_data_Pji_viNFJQ.csv']
test_datalab_path = "unit6-ch01-test"


# Function to load and preprocess data from CSV
def load_data_from_csv(paths):
    data = pd.DataFrame()
    for path in paths:
        data_1 = pd.read_csv(path)
        data_1['array'] = data_1['array'].apply(ast.literal_eval)  # Convert string to list
        data = pd.concat([data, data_1], ignore_index=True)
    return data

# Load and prepare the training data
train_data = load_data_from_csv(train_path)
train_images = np.array(train_data['array'].tolist())  # Convert list of lists to 2D numpy array
train_scores = train_data['score'].values

# Load and prepare the test data
# test_data = load_data_from_csv(test_path)
# test_images = np.array(test_data['array'].tolist())  # Convert list of lists to 2D numpy array
# test_scores = test_data['score'].values

# Normalize the pixel values to the range [0, 1]
train_images = train_images / 255.0
# test_images = test_images / 255.0

# Split the training data into training and validation sets
X_train, X_val, y_train, y_val = train_test_split(train_images, train_scores, test_size=0.2, random_state=42)

def build_model(hp):
    model = Sequential([
        Flatten(input_shape=(784,)),  # Flatten the input (28x28 pixels) to a 1D array
        Dense(hp.Int('units1', min_value=64, max_value=256, step=32), activation='relu'),  # First hidden layer with variable neurons and ReLU activation
        Dense(hp.Int('units2', min_value=32, max_value=128, step=32), activation='relu'),  # Second hidden layer with variable neurons and ReLU activation
        Dense(1)  # Output layer with a single neuron for regression
    ])
    model.compile(optimizer=Adam(learning_rate=hp.Choice('learning_rate', [0.001, 0.01, 0.1])), loss='mean_squared_error', metrics=['mean_squared_error'])
    return model


tuner = RandomSearch(
    build_model,
    objective='val_mean_squared_error',
    max_trials=10,
    executions_per_trial=1,
    directory='tuner_results',
    project_name='hyperparameter_tuning'
)

tuner.search(X_train, y_train, epochs=20, validation_data=(X_val, y_val))

# Get the best model
best_model = tuner.get_best_models(num_models=1)[0]

# Train the best model on the full training data
best_model.fit(X_train, y_train, epochs=20, validation_data=(X_val, y_val))

# Evaluate the best model on the validation data
val_predictions = best_model.predict(X_val)
val_mse = mean_squared_error(y_val, val_predictions)
print(f'Validation MSE: {val_mse}')

# Compile the model

# Train the model
best_model.fit(X_train, y_train, epochs=20, validation_data=(X_val, y_val))

y_pred_test = best_model.predict(X_val).flatten()

score = best_model.evaluate(X_val, y_val, verbose=0)
print(score)


# Predict on the test set

# Compute and print the mean squared error on the test set
mse_test = mean_squared_error(y_val, y_pred_test)
print(f'Test MSE: {mse_test}')

# classify test data
def get_image_bytes(image_path):
    with open(image_path, 'rb') as f:
        img_bytes = f.read()
        array = np.frombuffer(img_bytes, dtype=np.uint8)
        return array
    
def load_images_from_folder(folder):
    images = []
    filenames = []
    for filename in os.listdir(folder):
        img = get_image_bytes(os.path.join(folder, filename))
        if len(img) != 784:
                print(f"Error: The input image {filename} does not have 784 bytes.")
                continue
        images.append(img)
        filenames.append(filename)
            
    return images, filenames

images, filenames = load_images_from_folder(test_datalab_path)
images = np.array(images) / 255.0
pred_scores = best_model.predict(images).flatten()


with open("output1.csv", 'w', newline='') as csvfile:
        csvwriter = csv.writer(csvfile)
        for i, prediction in enumerate(pred_scores):
            csvwriter.writerow([filenames[i],prediction])

