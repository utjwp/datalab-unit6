import numpy as np
from scipy.optimize import lsq_linear
import os
import numpy as np
from skimage.io import imread
from skimage.color import rgb2gray
from skimage.transform import resize
from sklearn.svm import SVC
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report, accuracy_score
from sklearn.metrics import mean_squared_error
import cv2
file_path = 'stolen_two_class_data_oKlPl0FQHg.csv'

import pandas as pd

def load_images_from_folder():
    return pd.read_csv(file_path)


def train_svm(images, labels):
    # Split the data into training and testing sets
    X_train, X_test, y_train, y_test = train_test_split(images, labels, test_size=0.2, random_state=42)
    
    # Train the SVM model
    svm_model = SVC(kernel='linear', random_state=42)
    svm_model.fit(X_train, y_train)
    
    # Evaluate the model
    y_pred = svm_model.predict(X_test)
    print("Classification Report:\n", classification_report(y_test, y_pred))
    print("Accuracy:", accuracy_score(y_test, y_pred))
    
    return svm_model

# Example usage
folder_path = '/Users/sascharolinger/dev/datalab/datalab-unit6/ch01_train'  # Replace with the path to your folder containing images

data = load_images_from_folder()
imagess = data['array']
imagess = data['array']
svm_model = train_svm(images, labels)

def api_query(svm_model, sample):
    # Query the SVM model for the decision function value and label
    sample = sample.reshape(1, -1)
    # Query the SVM model for the decision function value and label
    decision_function_value = svm_model.decision_function(sample)[0]
    label = svm_model.predict(sample)[0]
    return decision_function_value, label


# Step 2: Collect samples
def collect_samples(num_samples):
    samples = []
    for _ in range(num_samples):
        sample = np.random.randint(0, 256, size=784, dtype=np.uint8)
        # Query the model
        decision_function_value, label = api_query(svm_model, sample)
        samples.append((sample, decision_function_value, label))
    return samples


def collect_samples_orth(num_samples, num_features=784):
    samples = []
    for i in range(num_samples):
        # Generate a sparse sample with exactly one non-zero value
        index = i
        value = 255  # Non-zero value (1 to 255)
        sample = np.zeros(num_features, dtype=np.uint8)
        sample[index] = value
        
        decision_function_value, label = api_query(svm_model, sample)
        
        samples.append((sample, decision_function_value, label))
    
    return samples


# Step 3: Solve for weights and bias
def solve_for_weights_and_bias(samples):
    X = []
    y = []
    for sample, decision_function_value, label in samples:
        X.append(sample)
        y.append(decision_function_value)
    
    X = np.array(X)
    y = np.array(y)
    
    # Add a column of ones to X for the bias term
    X = np.hstack((X, np.ones((X.shape[0], 1))))
    
    # Solve the linear system using least squares
    result = lsq_linear(X, y)
    
    weights_bias = result.x
    weights = weights_bias[:-1]
    bias = weights_bias[-1]
    
    return weights, bias




# Main function to steal the model
def steal_svm_model(num_samples):
    samples = collect_samples(num_samples)
    weights, bias = solve_for_weights_and_bias(samples)
    return weights, bias



# Example usage
num_samples = 1000    # Number of samples to collect
weights, bias = steal_svm_model(num_samples)
print("Weights stolen:", weights)
print("Bias stolen:", bias)


# Intercept (bias) added to the decision function
coefficients = svm_model.coef_

# Intercept (bias) added to the decision function
intercept = svm_model.intercept_
print("Coefficients (Weights) orginal:")
print(coefficients)
print("Intercept (Bias) orginal:")
print(intercept)

#weights = svm_model.coef_.flatten()  # Assuming SVM is linear, weights are coef_
#bias = svm_model.intercept_[0]  # Bias is intercept_


X_test = images[-100:]  # Example: Use the last 100 images as test data
y_test = labels[-100:]

# Original SVM predictions
y_pred_original = svm_model.decision_function(X_test)

# Stolen SVM predictions
y_pred_stolen = np.dot(X_test, weights) + bias


print("Stolen Decision Function Values:", y_pred_stolen)

# Compute MSE
mse = mean_squared_error(y_pred_original, y_pred_stolen)
print("mse:")
print(mse)