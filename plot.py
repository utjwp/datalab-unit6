import os
import numpy as np
import matplotlib.pyplot as plt
from skimage.io import imread
from skimage.transform import resize
import random

# Function to get image bytes and ensure they are 28x28 pixels
def get_image_bytes(image_path):
   with open(image_path, 'rb') as f:
        img_bytes = f.read()
        array = np.frombuffer(img_bytes, dtype=np.uint8)
        return array

# Function to load images from a folder
def load_images_from_folder(folder):
    images = []
    filenames = []
    for filename in os.listdir(folder):
        img = get_image_bytes(os.path.join(folder, filename))
        if len(img) != 784:
            print(f"Error: The input image {filename} does not have 784 bytes.")
            continue
        images.append(img)
        filenames.append(filename)
    return images, filenames

# Function to plot images
def plot_images(images, num_images=10, images_per_row=10):
    num_rows = (num_images // images_per_row) + (1 if num_images % images_per_row else 0)
    plt.figure(figsize=(images_per_row, num_rows))
    for i in range(num_images):
        plt.subplot(num_rows, images_per_row, i + 1)
        img = images[i].reshape(28, 28)  # Reshape to 28x28 pixels
        plt.imshow(img, cmap='gray')
        plt.axis('off')
    plt.tight_layout()
    plt.show()

# Main code
folder = "unit6-ch01-test"  # Replace with the path to your folder containing images
images, filenames = load_images_from_folder(folder)

# Plot some of the images
num_images_to_plot = 200  # Total number of images to plot
random_images = random.sample(images, num_images_to_plot)

# Plot the randomly selected images
plot_images(random_images, num_images=num_images_to_plot, images_per_row=20)
