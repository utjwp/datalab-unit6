import requests
import os

API_KEY = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoicmVmcmVzaCIsImV4cCI6MTc0NTY4NTU5MywianRpIjoiNmY2M2IwNTAzMzI1NDEyNDk5MmUwN2E0MTE5YjBjY2MiLCJ1c2VyX2lkIjo5fQ.d0fDnppqsDLvQJMdmeohohEaS_EfN-zLxoKlPl0FQHg"
URL = "http://datalab.intellisec.de/unit6/ch01"  # Replace with your actual URL
INPUT = "/tmp/test"

def query_stats():
    response = requests.post(f"{URL}/stats", data={"auth": API_KEY})
    print(response.text)

def predict_sample():
    # Generate random data
    with open(INPUT, 'wb') as f:
        f.write(os.urandom(784))
    
    # Predict sample
    with open(INPUT, 'rb') as f:
        response = requests.post(f"{URL}/predict", files={"input": f}, data={"auth": API_KEY})
        print(response.text)

if __name__ == "__main__":
    print("[*] Query stats...")
    query_stats()
    print("")
    
    print("[*] Predict sample...")
    predict_sample()
    print("")
    
    print("[*] Query stats again...")
    query_stats()