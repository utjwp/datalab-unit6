import numpy as np
from scipy.optimize import lsq_linear
import os
import numpy as np
from skimage.io import imread
from skimage.color import rgb2gray
from skimage.transform import resize
from sklearn.svm import SVC
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report, accuracy_score
from sklearn.metrics import mean_squared_error
import cv2
import ast
from PIL import Image
import csv
train_path = 'stolen_two_class_data_oKlPl0FQHg.csv'
test_path = 'stolen_two_class_data_Pji_viNFJQ.csv'

test_datalab_path = "unit6-ch01-test"

import pandas as pd

def load_images_from_folder(path):
    return pd.read_csv(path)

def convert_to_array(array_str):
    return ast.literal_eval(array_str)

data = load_images_from_folder(train_path)
images = data['array'].apply(convert_to_array)
scores = data['score']

# Step 3: Solve for weights and bias
def solve_for_weights_and_bias(samples):
    X = []
    y = []
    for sample, decision_function_value in samples:
        X.append(sample)
        y.append(decision_function_value)
    
    X = np.array(X)
    y = np.array(y)
    
    # Add a column of ones to X for the bias term
    X = np.hstack((X, np.ones((X.shape[0], 1))))
    
    # Solve the linear system using least squares
    result = lsq_linear(X, y)
    
    weights_bias = result.x
    weights = weights_bias[:-1]
    bias = weights_bias[-1]
    
    return weights, bias

# Main function to steal the model

samples = zip(images.values, scores.values)
weights, bias = solve_for_weights_and_bias(samples)


print("Weights stolen:", weights)
print("Bias stolen:", bias)

test_data = load_images_from_folder(test_path)
test_images = np.array(data['array'].apply(convert_to_array).tolist())
test_scores = test_data['score']


# Stolen SVM predictions
print(test_images.shape)
print(weights.shape)
print(bias.shape)

y_pred_stolen = np.dot(test_images, weights) + bias


print("Stolen Decision Function Values:", y_pred_stolen)

# Compute MSE
mse = mean_squared_error(test_scores, y_pred_stolen)
print("mse:")
print(mse)


# classify test data
def get_image_bytes(image_path):
    # with Image.open(image_path) as img:
    #     img = img.convert('L')  # Ensure image is in grayscale
    #     img = img.resize((28, 28))  # Ensure image is 28x28 pixels
    #     img_bytes = img.tobytes()
    #     return img_bytes
    with open(image_path, 'rb') as f:
        img_bytes = f.read()
        array = np.frombuffer(img_bytes, dtype=np.uint8)
        return array
    
def load_images_from_folder(folder):
    images = []
    filenames = []
    for filename in os.listdir(folder):
        img = get_image_bytes(os.path.join(folder, filename))
        if len(img) != 784:
                print(f"Error: The input image {filename} does not have 784 bytes.")
                continue
        images.append(img)
        filenames.append(filename)
            
    return images, filenames

images, filenames = load_images_from_folder(test_datalab_path)
images = np.array(images)
pred_scores = np.dot(images, weights) + bias


with open("output1.csv", 'w', newline='') as csvfile:
        csvwriter = csv.writer(csvfile)
        for i, prediction in enumerate(pred_scores):
            csvwriter.writerow([filenames[i],prediction])

