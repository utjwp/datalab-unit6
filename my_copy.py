import os
import shutil

def copy_files(src_folder, dst_folder):
    # Create destination folder if it doesn't exist
    if not os.path.exists(dst_folder):
        os.makedirs(dst_folder)
    
    # Iterate over all files in the source folder
    for filename in os.listdir(src_folder):
        if filename.endswith('8') or filename.endswith('5'):
            src_file = os.path.join(src_folder, filename)
            dst_file = os.path.join(dst_folder, filename)
            
            # Copy the file
            shutil.copy2(src_file, dst_file)

# Example usage
src_folder = '/Users/sascharolinger/dev/datalab/datalab-unit5/ch01_train'  # Replace with your source folder path
dst_folder = '/Users/sascharolinger/dev/datalab/datalab-unit6/ch01_train'  # Replace with your destination folder path

copy_files(src_folder, dst_folder)